const _ = require('lodash')
module.exports = function(elements, interval){
    if(!(args.elements instanceof NodeList))
        throw new Error('Invalid elements')

    var delay = 0
    
    _.forEach(args.elements, function(element, key){
        if(_.includes(element.classList, 'group'))
            return module.exports(element, interval)

        var finish = false

        _.forEach(['no-animation', 'navigation-buttons'], function(className){
            if(_.includes(element.classList), className){
                finish = true
                return false
            }
        })

        if(finish === true) return true

        var animationClasses = ''
        const classes = element.getAttribute('data-ani-class')
        if(_.isString(classes) && classes.length){
            animationClasses = classes
        }
        else{
            animationClasses = 'fadeInUp'
        }
    
        var _delay = element.getAttribute('data-ani-delay')
        if(!_.isString(_delay) || !_delay.length){
            _delay = delay
        }
    
        const attributes = {
            'data-ani-class': animationClasses,
            'data-ani-delay': _delay
        }

        _.forEach(attributes, function(value, attribute){
            element.setAttribute(attribute, value)
        })

        delay+= _.isNumber(interval) ? interval : 250
    })

    return true
}